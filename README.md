# Unit Testing Example

This repository contains a simple example of unit testing in Java. It is based on the Edabit challenge 
[Maximum Distance](https://edabit.com/challenge/6cBQuxkvN4f2Qn8x9) by [Mateusz Mędrowski](https://edabit.com/user/hPx9H3YM8j2k94DNW).

The video can be found at (tbd).

## What does this class do?

This class implements a simple vehicle range calculator. It takes the following inputs:
* tankSize: The size of the tank in liters
* consumption: The fuel consumption in liters per 100 km
* numberOfPassengers: The number of passengers in the vehicle (without the driver)
* climateControlActivated: Whether the climate control is on or off

The rules are: 
* The fuel consumption is increased by 5% for each person in the car (driver plus passengers), in relation to the base consumption
* The fuel consumption is increased by 10% if the climate control is on, in relation to the base consumption


## "main" Branch: Some bugs "on purpose"

The main branch contains some bugs on purpose. The goal is to find and fix them using unit tests. These bugs are:
* The driver is not counted as a person in the car
* The rule "10% increase if the climate control is on" is not implemented
* Edge and out-of-bounds cases are not handled correctly (e.g. negative tank size, zero consumption, etc.)

There are a number of unit tests in the `test` folder. These tests try to cover the different cases:
* Each execution path is covered by at least one test
* Each rule is covered by at least one test
* Each edge case is covered by at least one test
* Each out-of-bounds case is covered by at least one test

## "bugs_fixed" Branch: All bugs fixed

The solution branch contains a solution to the bugs. All unit tests pass.  
