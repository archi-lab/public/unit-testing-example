package thkoeln.archilab.drivingrange;

/**
 * Class to calculate the kilometer range of a vehicle, given some parameters
 */
public class Vehicle {

    // Class variables
    private float tankSize = 50.0f;
    private float consumption = 8.0f;
    private int numberOfAdditionalPassengers = 0;
    private boolean climateControlActivated = false;

    /**
     * Main method to calculate the range of a vehicle
     * @param args Command line arguments
     *             tankSize: Size of the tank in liters
     *             consumption: Consumption of the vehicle in liters per 100km
     *             numberOfPassengers: Number of passengers in the vehicle
     *             climateControl: true if climate control is activated, false otherwise
     */
    public static void main( String[] args ) {
        if ( args.length != 4 ) {
            System.out.println( "ERROR: 4 parameters needed, <tankSize> <consumption> <numberOfPassengers> <climateControl>" );
            return;
        }
        Vehicle vehicle = new Vehicle(
                Float.parseFloat( args[0] ), Float.parseFloat( args[1] ),
                Integer.parseInt( args[2] ), Boolean.parseBoolean( args[3] ) );
        float range = vehicle.calculateRange();
        System.out.println( "Calculated vehicle range: " + range + " kilometers" );
    }


    public Vehicle( float tankSize, float consumption, int numberOfAdditionalPassengers, boolean climateControlActivated ) {
        setTankSize( tankSize );
        setConsumption( consumption );
        setNumberOfAdditionalPassengers( numberOfAdditionalPassengers );
        setClimateControlActivated( climateControlActivated );
    }

    public Vehicle() {
    }


    /**
     * Method to calculate the range of a vehicle
     * @return Range of the vehicle in kilometers
     */
    public float calculateRange() {
        float actualConsumption = getConsumption();
        float increase = 1f;

        increase += ( getNumberOfAdditionalPassengers() ) * 0.05f;
        actualConsumption = actualConsumption * increase;

        float range = getTankSize() / actualConsumption * 100;
        return range;
    }


    public float getTankSize() {
        return tankSize;
    }

    public void setTankSize( float tankSize ) {
        this.tankSize = tankSize;
    }

    public float getConsumption() {
        return consumption;
    }

    public void setConsumption( float consumption ) {
        this.consumption = consumption;
    }

    public int getNumberOfAdditionalPassengers() {
        return numberOfAdditionalPassengers;
    }

    public void setNumberOfAdditionalPassengers( int numberOfAdditionalPassengers ) {
        this.numberOfAdditionalPassengers = numberOfAdditionalPassengers;
    }

    public boolean isClimateControlActivated() {
        return climateControlActivated;
    }

    public void setClimateControlActivated( boolean climateControlActivated ) {
        this.climateControlActivated = climateControlActivated;
    }
}
