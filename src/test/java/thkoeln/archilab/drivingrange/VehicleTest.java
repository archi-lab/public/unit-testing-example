package thkoeln.archilab.drivingrange;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for Vehicle.
 */
public class VehicleTest {

    private Vehicle muscleCar;
    private Vehicle familyVan;
    private float expectedRangeMuscleCar;
    private float expectedRangeFamilyVan;
    private float actualRangeMuscleCar;
    private float actualRangeFamilyVan;

    private final float DELTA = 0.01f; // Delta for float comparison
    private final float PERSON_INCREASE = 0.05f;
    private final float CLIMATE_CONTROL_INCREASE = 0.1f;

    /**
     * Set up the calculator instance before each test.
     */
    @BeforeEach
    public void setUp() {
        muscleCar = new Vehicle( 42f, 10f, 0, false );
        familyVan = new Vehicle( 78f, 6f, 3, true );

        // 1 person (driver) in the muscle car, no climate control, meaning 10,5l/100km (5% increase)
        expectedRangeMuscleCar = 400f;

        // 4 people (driver + 3 passengers) in the family van, climate control, meaning 30% increase
        // => 6 + 1,8 = 7,8l/100km, meaning 1000km range
        expectedRangeFamilyVan = 1000f;

    }

    @Test
    public void testFullConstructor() {
        // given / when
        Vehicle muscleCarCopy = new Vehicle( muscleCar.getTankSize(), muscleCar.getConsumption(),
                muscleCar.getNumberOfAdditionalPassengers(), muscleCar.isClimateControlActivated() );

        // then
        assertEquals( muscleCar.getTankSize(), muscleCarCopy.getTankSize(), DELTA );
        assertEquals( muscleCar.getConsumption(), muscleCarCopy.getConsumption(), DELTA );
        assertEquals( muscleCar.getNumberOfAdditionalPassengers(), muscleCarCopy.getNumberOfAdditionalPassengers() );
        assertEquals( muscleCar.isClimateControlActivated(), muscleCarCopy.isClimateControlActivated() );
    }



    @Test
    public void testBasicRangeCalculationOnlyDriverAndClimateOff() {
        // given / when
        actualRangeMuscleCar = muscleCar.calculateRange();
        actualRangeFamilyVan = familyVan.calculateRange();

        // then
        assertEquals( expectedRangeMuscleCar, actualRangeMuscleCar, DELTA );
        assertEquals( expectedRangeFamilyVan, actualRangeFamilyVan, DELTA );
    }


    @Test
    public void testConsumptionIncreaseForAdditionalPassengers() {
        // given
        muscleCar.setNumberOfAdditionalPassengers( muscleCar.getNumberOfAdditionalPassengers() + 2 );
        familyVan.setNumberOfAdditionalPassengers( familyVan.getNumberOfAdditionalPassengers() + 1 );

        // when
        expectedRangeMuscleCar = expectedRangeMuscleCar * ( 1 + 1 * PERSON_INCREASE ) / ( 1 + 3 * PERSON_INCREASE );
        expectedRangeFamilyVan = expectedRangeFamilyVan * ( 1 + 4 * PERSON_INCREASE + CLIMATE_CONTROL_INCREASE )
                / ( 1 + 5 * PERSON_INCREASE + CLIMATE_CONTROL_INCREASE );
        actualRangeMuscleCar = muscleCar.calculateRange();
        actualRangeFamilyVan = familyVan.calculateRange();

        // then
        assertEquals( expectedRangeMuscleCar, actualRangeMuscleCar, DELTA );
        assertEquals( expectedRangeFamilyVan, actualRangeFamilyVan, DELTA );
    }


    @Test
    public void testConsumptionIncreaseWithClimateControl() {
        // given
        muscleCar.setClimateControlActivated( true );

        // when
        expectedRangeMuscleCar = expectedRangeMuscleCar * ( 1 + 1 * PERSON_INCREASE ) /
                ( 1 + 1 * PERSON_INCREASE + CLIMATE_CONTROL_INCREASE );
        actualRangeMuscleCar = muscleCar.calculateRange();

        // then
        assertEquals( expectedRangeMuscleCar, actualRangeMuscleCar, DELTA );
    }


    @Test
    public void testEdgeParameters() {
        // given
        muscleCar.setTankSize( 0f );

        // when
        actualRangeMuscleCar = muscleCar.calculateRange();

        // then
        assertEquals( 0f, actualRangeMuscleCar, DELTA );

    }


    @Test
    public void testOutOfBoundsInput() {
        // given / when / then
        assertThrows( IllegalArgumentException.class, () -> { muscleCar.setTankSize( -1f ); } );
        assertThrows( IllegalArgumentException.class, () -> { muscleCar.setConsumption( 0f ); } );
        assertThrows( IllegalArgumentException.class, () -> { muscleCar.setConsumption( -1f ); } );
        assertThrows( IllegalArgumentException.class, () -> { muscleCar.setNumberOfAdditionalPassengers( -1 ); } );
        // the maximum number of passengers is 8 in a non-commercial vehicle
        assertThrows( IllegalArgumentException.class, () -> { muscleCar.setNumberOfAdditionalPassengers( 9 ); } );
    }


}
